# Drex
Drex is a music player for Android based on Google's Material Design standards. The point of Drex is to be a simple, lightweight media player designed for normal people (meaning that if you need an EQ, gapless playback, crossfade, or replay gain, Drex probably isn't for you) Or you can probably use any system inbuilt EQ.

### Permissions
** Read and write to external storage **  
Used to save local data; primarily used for Last.fm cache, library storage, and other small miscellaneous files.  
** Internet **  
Used to retrieve information and thumbnails for artists from Last.fm  
** Network State **  
Used to prevent Drex from using mobile data (if this preference is enabled)
** Keep awake **  
Used to play music while the device's screen is off  
** Install and Uninstall shortcuts **  
Allows Drex to (optionally) add shortcuts to Drex to the launcher. This is only done when explicitly requested from the settings page and is intended so that the launcher icon matches the chosen theme

### Setting up the project
 - Download, install and launch [Android Studio]
 - Clone the repository
 - In Android Studio, select "Import Project..." from the file menu
 - Select Drex's repository that you just cloned
 - Wait while Gradle begins to build the project (Android Studio may need to restart)

### Bugs & contributing
Feel free to post suggestions, crashes, or just anything that isn't as smooth as it should be to the bug tracker -- I want Drex to be as seamless as it possibly can. Additionally, don't hesitate to fork Drex or submit a pull request -- especially if it's a bug fix or cleans up code that's doing mischevious, devious or otherwise bad things that I'm not aware of.

### License
Drex is licensed under an Apache 2.0 license