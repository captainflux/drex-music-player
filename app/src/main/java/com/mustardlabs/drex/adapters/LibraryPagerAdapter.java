package com.mustardlabs.Drex.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentPagerAdapter;

import com.mustardlabs.Drex.R;
import com.mustardlabs.Drex.fragments.AlbumFragment;
import com.mustardlabs.Drex.fragments.ArtistFragment;
import com.mustardlabs.Drex.fragments.GenreFragment;
import com.mustardlabs.Drex.fragments.PlaylistFragment;
import com.mustardlabs.Drex.fragments.SongFragment;

public class LibraryPagerAdapter extends FragmentPagerAdapter {
    private final static int NUM_ITEMS = 5;
    private PlaylistFragment playlistFragment;
    private SongFragment songFragment;
    private ArtistFragment artistFragment;
    private AlbumFragment albumFragment;
    private GenreFragment genreFragment;
    private FragmentActivity activity;

    public LibraryPagerAdapter(FragmentActivity activity) {
        super(activity.getSupportFragmentManager());
        this.activity = activity;
    }

    @Override
    public Fragment getItem(int position) {
        switch (position) {
            case 0:
                if (playlistFragment == null) {
                    playlistFragment = new PlaylistFragment();
                }
                return playlistFragment;
            case 1:
                if (songFragment == null) {
                    songFragment = new SongFragment();
                }
                return songFragment;
            case 2:
                if (artistFragment == null) {
                    artistFragment = new ArtistFragment();
                }
                return artistFragment;
            case 3:
                if (albumFragment == null) {
                    albumFragment = new AlbumFragment();
                }
                return albumFragment;
            case 4:
                if (genreFragment == null) {
                    genreFragment = new GenreFragment();
                }
                return genreFragment;
            default:
                return null;
        }
    }

    @Override
    public int getCount() {
        return NUM_ITEMS;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        switch (position) {
            case 0:
                return activity.getResources().getString(R.string.header_playlists);
            case 1:
                return activity.getResources().getString(R.string.header_songs);
            case 2:
                return activity.getResources().getString(R.string.header_artists);
            case 3:
                return activity.getResources().getString(R.string.header_albums);
            case 4:
                return activity.getResources().getString(R.string.header_genres);
            default:
                return "Page " + position;
        }
    }

    public void refreshPlaylists() {
        if(playlistFragment != null) playlistFragment.updateData();
    }
}
