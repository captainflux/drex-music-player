package com.mustardlabs.Drex.fragments;

import android.os.Bundle;
import android.os.Parcelable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;

import com.mustardlabs.Drex.R;
import com.mustardlabs.Drex.adapters.GenreListAdapter;
import com.mustardlabs.Drex.adapters.SearchPagerAdapter;
import com.mustardlabs.Drex.instances.Genre;
import com.mustardlabs.Drex.utils.Themes;

import java.util.ArrayList;

public class GenreFragment extends Fragment {

    private ArrayList<Genre> genreLibrary;
    private GenreListAdapter adapter;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if (getArguments() != null && getArguments().getParcelableArrayList(SearchPagerAdapter.DATA_KEY) != null){
            genreLibrary = new ArrayList<>();
            for (Parcelable p : getArguments().getParcelableArrayList(SearchPagerAdapter.DATA_KEY)){
                genreLibrary.add((Genre) p);
            }
        }

        if (genreLibrary == null) {
            adapter = new GenreListAdapter(getActivity());
        } else {
            adapter = new GenreListAdapter(genreLibrary, getActivity());
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_list, container, false);
        ListView genreListView = (ListView) view.findViewById(R.id.list);

        int paddingTop = (int) getActivity().getResources().getDimension(R.dimen.list_margin);
        int paddingH =(int) getActivity().getResources().getDimension(R.dimen.global_padding);
        view.setPadding(paddingH, paddingTop, paddingH, 0);

        genreListView.setAdapter(adapter);
        genreListView.setOnItemClickListener(adapter);
        genreListView.setBackgroundColor(Themes.getBackgroundElevated());

        return view;
    }

    public void updateData(ArrayList<Genre> genreLibrary) {
        this.genreLibrary = genreLibrary;
        adapter.updateData(genreLibrary);
    }
}
