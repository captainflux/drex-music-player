package com.squareup.picasso;

public class PicassoUtils {

    public static void clearCache (Picasso p) {
        p.cache.clear();
    }

}